#include <windows.h>
#include <nathan/utils.h>
#include <stdio.h>

#define LANG_DEFAULT MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT)

void 
doerror(const char *prelim)
{
    DWORD err = GetLastError();
    size_t errlen = 1024;
    char *errmsg = MALLOC(char, errlen);

    FormatMessage(
        FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        err,
        LANG_DEFAULT,
        errmsg,
        errlen,
        NULL
    );

    fprintf(stderr, "%s: %s", prelim, errmsg);
    free(errmsg);
}

VOID
WINAPI
closeservice(SERVICE_STATUS_HANDLE hand, LPSERVICE_STATUS stat, int cp)
{
    stat->dwControlsAccepted = 0;
    stat->dwWin32ExitCode = GetLastError();
    stat->dwCurrentState = SERVICE_STOPPED;
    stat->dwCheckPoint = cp;
    SetServiceStatus(hand, stat);
}
