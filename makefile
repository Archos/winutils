
NAME = winutils

INCDIR = /usr/include/nathan
LIBDIR = /usr/lib/nathan

install: $(INCDIR) $(LIBDIR) build $(NAME).h
	cp lib$(NAME).a $(LIBDIR)/lib$(NAME).a
	cp $(NAME).h $(INCDIR)/$(NAME).h

uninstall:
	rm -f $(INCDIR)/$(NAME).h $(LIBDIR)/$(NAME).a

$(INCDIR):
	mkdir $(INCDIR)

$(LIBDIR):
	mkdir $(LIBDIR)

build: $(NAME).c
	cc -c $(NAME).c
	ar rcs lib$(NAME).a $(NAME).o

clean:
	rm -f *.o *.a
